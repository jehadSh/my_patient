<?php

namespace App\Http\Controllers\App_Mobile;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctor = Doctor::where('id', $id)->first();
        // $this->authorize('view', $role);
        return response()->json($doctor, 200);
    }


    public function update(Request $request, $id)
    {
        $doctor = Doctor::where('id', $id)->first();
        if (!$doctor) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('update', $user);
        $doctor->update($request->all());
        return response()->json($doctor, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doctor = Doctor::where('id', $id)->first();
        if (!$doctor) {
            return response()->json('It does not exist actually', 200);
        }
        // $this->authorize('delete', $user);
        $doctor = $doctor->delete();

        return response()->json('Done Delete user', 200);
    }
}
