<?php

namespace App\Http\Controllers\App_Mobile;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function registerDoctor(Request $request)
    {

        $doctor = Doctor::create($request->all());
        $doctor->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $doctor->createToken('my-app-token')->plainTextToken;
        $response = ['Doctor' => $doctor, 'token' => $token];
        return response()->json($response, 201);
    }
    public function registerPatient(Request $request)
    {

        $patient = Patient::create($request->all());
        $patient->update([
            'password' => bcrypt($request->password),
        ]);
        $token = $patient->createToken('my-app-token')->plainTextToken;
        $response = ['Patient' => $patient, 'token' => $token];
        return response()->json($response, 201);
    }
    public function loginDoctor(Request $request)
    {
        if ($request->phone == null || $request->password == null) {
            return response()->json('please enter your phone or your password', 201);
        }
        $fields = $request->validate([
            'phone' => 'required',
            'password' => 'required|string'
        ]);
        // Check phone
        $doctor = Doctor::where('phone', $fields['phone'])->first();

        // Check password
        if (!$doctor || !Hash::check($fields['password'], $doctor->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }

        $token = $doctor->createToken('my-app-token')->plainTextToken;

        $response = [
            'Doctor' => $doctor,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

    public function loginPatient(Request $request)
    {
        if ($request->phone == null || $request->password == null) {
            return response()->json('please enter your phone or your password', 201);
        }
        $fields = $request->validate([
            'phone' => 'required',
            'password' => 'required|string'
        ]);
        // Check phone
        $patient = Patient::where('phone', $fields['phone'])->first();

        // Check password
        if (!$patient || !Hash::check($fields['password'], $patient->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }

        $token = $patient->createToken('my-app-token')->plainTextToken;

        $response = [
            'Patient' => $patient,
            'token' => $token
        ];

        return response()->json($response, 201);
    }

    public function logout(Request $request)
    {
        Auth::user()->tokens()->delete();
        return response()->json('Logged out', 200);
    }
    public function test()
    {
        return "test";
    }

    // public function isToken(Request $request)
    // {
    //     $user = Auth::user();
    //     if ($user) {
    //         return response()->json($user, 200);
    //     } else {
    //         return response()->json('not login', 404);
    //     }

    // }

    // public function editPassword(Request $request)
    // {
    //     $user = Auth::user();
    //     $fields = $request->validate([
    //         'password' => 'required|string'
    //     ]);
    //     if (!$user || !Hash::check($fields['password'], $user->password)) {
    //         return response(['message' => 'كلمة السر خاطئة'], 401);
    //     }
    //     $user->update([
    //         'password' => bcrypt($request->newPassword),
    //     ]);
    //     return response()->json('Done ', 200);
    // }
}
