<?php

namespace App\Http\Controllers;

use App\Models\OrderPatient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class PatientController extends Controller
{

    public function index()
    {
        $order = OrderPatient::where('id', '<>', 0)->get();
        if (!$order) {
            return response()->json('It does not exist actually', 200);
        } else {
            return response()->json($order, 200);
        }
    }

    public function store(Request $request)
    {
        $doctor_id = Auth::user()->id;

        if ($request->order_id) {
            $order = OrderPatient::where('id', $request->order_id)->first();
            // return $order;
            $order->update([
                'name' => $request->name ?? $order->name,
                'age' => $request->age ?? $order->age,
                'gender' => $request->gender ?? $order->gender,
                'phone' => $request->phone ?? $order->phone,
                'favorite_gender' => $request->favorite_gender ?? $order->favorite_gender,
                'category' => $request->category ?? $order->category,
            ]);
            if ($request->hasFile('photo')) {
                $path = storage_path('app/public/photo_1/') . $order->path_photo_1;
                unlink($path);
                $new_photo = $request->file('photo');
                $order->name_photo_1 = $new_photo->getClientOriginalName();
                $photo_path = uniqid() . '.' . $new_photo->getClientOriginalExtension();
                $path = $new_photo->storeAs('public/photo_1', $photo_path);
                $order->path_photo_1 = $path;
                $url = Storage::url($path);
                $order->url_photo_1 = $url;
            }
            $order->save();
            return response()->json($order, 200);
        } else {
            $order = new OrderPatient();

            $order->name = $request->name;
            $order->age = $request->age;
            $order->gender = $request->gender;
            $order->phone = $request->phone;
            $order->favorite_gender = $request->favorite_gender;
            $order->favorite_university = $request->favorite_university;
            $order->category = $request->category;
            $order->doctor_id = $doctor_id;

            if ($request->hasFile('photo')) {
                $photo = $request->file('photo');
                $order->name_photo_1 = $photo->getClientOriginalName();
                $photo_path = uniqid() . '.' . $photo->getClientOriginalExtension();
                $path = $photo->storeAs('public/photo_1', $photo_path);
                $order->path_photo_1 = $photo_path;
                $url = Storage::url($path);
                $order->url_photo_1 = $url;

            }
            $order->save();

            return response()->json($order, 201);
        }
    }

    public function show($id)
    {
        $order = OrderPatient::where('id', $id)->first();

        if (!$order) {
            return response()->json('It does not exist actually', 200);
        }
        return response()->json($order, 200);
    }

    public function update(Request $request, $id)
    {
        // $order = OrderPatient::where('id', $id)->first();

        // if (!$order) {
        //     return response()->json('It does not exist actually', 200);
        // }
    }

    public function destroy($id)
    {
        //
    }
}