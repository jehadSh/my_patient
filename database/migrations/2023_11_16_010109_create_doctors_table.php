<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('password');
            $table->integer('otp')->nullable();
            $table->bigInteger('phone');
            $table->integer('user_point')->default(0);
            $table->integer('user_money')->default(0);
            $table->integer('age');
            $table->enum('gender', ['male', 'famle']);
            $table->tinyInteger('student_year');
            $table->enum('university', ['Dams', 'Homes']);
            $table->string('photo')->nullable();
            $table->longText('favorite')->nullable();
            $table->enum('status', ['bloced', 'activited', 'OnHold'])->default('OnHold');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}