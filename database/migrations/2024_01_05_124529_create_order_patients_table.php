<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_patients', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->integer('age');
            $table->enum('gender', ['male', 'famle']);
            $table->bigInteger('phone');

            $table->longText('chronic_diseases')->nullable();
            $table->longText('medical_information')->nullable();
            $table->string('name_photo_1')->nullable();
            $table->longText('path_photo_1')->nullable();
            $table->longText('url_photo_1')->nullable();
            // $table->string('name_photo_2')->nullable();
            // $table->string('path_photo_2')->nullable();
            // $table->string('name_X_Ray')->nullable();
            // $table->string('path_X_Ray')->nullable();
            // $table->enum('university', ['Dams', 'Homes']);
            $table->enum('favorite_gender', ['male', 'famle']);
            $table->enum('favorite_university', ['Dams', 'Homes']);
            $table->string('category');
            $table->unsignedBigInteger('doctor_id')->nullable();

            $table->enum('status', ['bloced', 'activited', 'OnHold'])->default('OnHold');

            $table->timestamps();

            $table->foreign('doctor_id')->references('id')->on('doctors');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_patients');
    }
}