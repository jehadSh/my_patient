<?php

namespace Database\Seeders;

use App\Models\Doctor;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'name' => 'Jehad',
                'password' => Hash::make('12345678'),
                'phone' => "0934151507",
                'user_point' => 0,
                'user_money' => 0,
                'age' => 25,
                'gender' => 'male',
                'student_year' => 5,
                'university' => 'Dams',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ]
            // [
            //     'name' => 'osama',
            //     'email' => 'osama@gmail.com',
            //     'password' => Hash::make('12345678'),
            //     'created_at' => '2021-10-12',
            //     'updated_at' => '2021-10-12',
            // ],
            // [
            //     'name' => 'mh',
            //     'email' => 'mh@gmail.com',
            //     'password' => Hash::make('12345678'),
            //     'created_at' => '2021-10-12',
            //     'updated_at' => '2021-10-12',
            // ]
        ];

        Doctor::insert($user);
    }
}
