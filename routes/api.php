<?php

use App\Http\Controllers\App_Mobile\AuthController;
use App\Http\Controllers\App_Mobile\DoctorController;
use App\Http\Controllers\PatientController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post("/register_doctor", [AuthController::class, 'registerDoctor']);
Route::post("/register_patient", [AuthController::class, 'registerPatient']);
Route::post("/login_doctor", [AuthController::class, 'loginDoctor']);
Route::post("/login_patient", [AuthController::class, 'loginPatient']);
// Route::get("/test", [AuthController::class, 'test']);


Route::get('test/{id}', [PatientController::class, 'test']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post("/logout", [AuthController::class, 'logout']);
    Route::resource('doctors', DoctorController::class);
    Route::resource('order_patient', PatientController::class);
    Route::get("/download_photo/{id}", [PatientController::class, 'downloadFile']);


});
